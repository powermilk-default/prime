# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other
method with the owners of this repository before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Smart branching

### Presentation of the concept

In this repository, efforts were made to implement _smart branching_, the proposed project management methodology by
Atlassian (alternate names include "Gitflow Workflow" or "Smart Git"). The procedure is described in
this [article] (https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). The action graph
translates in short, like this:

```mermaid
sequenceDiagram
  participant M as Master
  participant R as Release
  participant D as Development
  participant F as Feature
  participant H as Hotfix
	M->>+R: Preparation of the next production version
  R->>+D: Merge for the development branch
	D->>+F: Start working on feature
        F->>F: Commit 1
        F->>F: Commit 2
        F->>F: Commit 3
  F->>-D: Completion of work on feature
  Note over F,D: Another new feature
  M->>+H: Bug detection
  H->>D: Bug fix
  H->>-M: Bug fix and version change
  D->>-R: Decision to close the release
  R->>-M: Uploading to production
```

Working with this methodology is facilitated by a Git add-on called GitFlow (the installation method explains
[this](https://github.com/petervanderdoes/gitflow-avh/wiki/Installation) article).

### Branch overview

After installation, initiate `git flow` by entering the command in the terminal

```bash
git flow init
```

and name the functional branches accordingly. The point is followed by a functional one (nomenclature as presented
earlier of the article) the name of the branch, and the representative name in the project in parentheses.

* Development branch (_development_) — The main developer branch. All changes intended for the next release
  (_release branch_) they will get here by direct `commit` of small changes or` merge` of larger functionality
  (_feature branch_)
* Production branch (_master_) — This branch represents the latest implementation in production. Updated by linking
  other branches.
* Feature branch (_feature_) — Branch designed to work on the feature, with greater functionality and a non-trivial
  issue. After the code is finished, this branch should be merged into the `Development` branch, where it waits for
  the "release" closure and implementation for production. The name `feature/` is only a prefix to the name of the
  appropriate branch. The convention adopted in the project is `reported.bug.number-short-branch-description`,
  e.g.` 54321-add-search-engine`, where `reported.bug.number` is the number of the bug reported in [_Issue
  board_](https://gitlab.com/rafal.kociniewski/exchangerate/-/issues). If we are performing a task unrelated to any
  reported issue, this part can be skipped. So in these examples, the name of the new Feature branch will be `
  feature/54321-add-search-engine` or` feature/add-search-engine`.
* Release branch (_release_) — This branch is for creating a new release. It is created from the current
  branch `development` and closed after deciding to publish on the production server. When you are finished with this
  branch, an appropriate `tag` is created and the branch is joined to the` master` branch. Due to the nature of the
  project, no work methodology has been adopted (SCRUM, Waterfall, Kanban, Lean etc.). Similarly to the previous
  example, `release/` is only a prefix to the proper branch name. This one will be related to the new release currently
  being worked on, e.g. `1.6.x` means that all changes to `feature` will be tagged with `1.6.0`,` 1.6.1`, `1.6.2` etc.
* Hotfix branch (_hotfix_) - A branch intended for quick fixes. These include uploading a patch code, "patching" serious
  defects found in production or repairing defects that prevent the correct operation of the program. Contrary to other
  branches, this one is based on `master`. After you finish working on the "patch", you need to do a merge to both the
  branch `master` (the patch must go to production) and to` development` (it must be in the next release - without
  overwriting the branch `master`). Similar to the previous case, the name `hotfix/` is also a prefix to the actual
  branch name. The nomenclature should be similar to the Feature branch,
  i.e. `reported.error.number-short-branch-description`, e.g.` 12345-nullpointerexception-protection`,
  where `reported.error.number` is the number of the bug reported in [_Issue
  board_](https://gitlab.com/rafal.kociniewski/exchangerate/-/issues). If we perform repairs unrelated to any reported
  problem, this part can be skipped. Thus, in the given examples, the name of the new branch Hotfix will be `
  hotfix/12345-nullpointerexception` or` hotfix/anti-nullpointerexception`.

## Code standards

This section covers how to work on the code, create comments, tests, etc.

* Most of the code is written in Kotlin. Try to follow the rules set out
  in [Kotlin Code Conventions](https://kotlinlang.org/docs/reference/coding-conventions.html).
* Integrate your IDE so that it can use the Detekt tool. It is a tool for analyzing Kotlin static code. The
  configuration of the checking convention is specified in `detekt.yml`.
* Code formatting rules are specified in `.editorconfig`. Make sure your IDE supports it.
* Document each created class, function, extension, constructor, enum, etc. The standard for creating documentation in
  Kotlin is KDoc (the Java equivalent is JavaDoc), which is generated by the Dokka utility. Use the `gradle dokka` to
  view the documentation flow. If as a result of generating errors of the type `[Warning] No documentation for (...)` (
  i.e. missing documentation for part of the code), fix them immediately.
* Documentation, descriptions, messages or `commit message` are written in English.
* Take care of the language! And I'm not just talking about the programming language ;). I know, I know, we want to
  create code and we are not necessarily omnibuses in the field of spelling or grammar of a language. Nevertheless,
  write communications and documentation so that it is understandable to outsiders. A suitable plugin for the IDE can be
  a good help (I personally use `Grazie` for Intellij which made most of the documentation easy for me).
* Remove comments - this is a redundant code element. If you chose not to include it in the documentation, it means that
  it is unnecessary. You most likely created them while testing or adding code to post your thoughts.
* Remove `println (...)`. You probably wanted to display something on the console while debugging or testing
  functionality.
* Create tests for your functionality! The application uses the `springmockk` and` JUnit 5` tools. Try to provide unit
  and integration testing.

## Merge request guidelines

* The project should be cloned or forked on the local machine before making changes.
* Use the above-described branch names. The purpose and operation of _smart branching_ are briefly described, for not
  included topics see [this](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) article. If
  this article doesn't cover all doubts on this topic use common sense in your decisions or contact the project owner.
* Make sure any build dependencies are removed before committing. It means you should check if you haven't included any
  directory, generated file or changes for testing in the commit. Consider whether such generated files should be
  in `.gitignore`.
* Update [README.md](README.md) and [CHANGELOG.md](CHANGELOG.md) with detailed changes.
* Increase the version in all the sample files and README.md to the new version that your _Merge request_
  represents. The versioning scheme we use is [SemVer](https://semver.org/lang/pl/).
* Remember that `feature/xxx` should be merged into `development`.
* You can make multiple commits on your branch, you don't have to squash them into one before _Merge request_. However,
  when creating a _Merge request_ check the box to make it squash after it commits the changes.
* The `squash commit message` convention is` [ver.number]Short description` and a more detailed description under the
  header. If the headline itself is meaningful, you can leave a detailed description, for
  example `[1.5.0] Correct typos in the documentation of the class Something`. An example of a good `commit message`:

```
[1.1.0] Adding e-mail sending by automata

A data class Mail has been created, which is an e-mail representation sent by the automa after posting a comment in a specific thread.
```

* It's a good idea to pull empty commit with message using `--allow empty` option,
  e.g.`git commit --allow-empty -m "[1.5.0] Correct typos in the documentation of the Something class"` to be able to
  select the correct squash commit message`
* _Merge request_ must be approved by the project owner or another person delegated to this function. Ideally, at least
  two programmers should review the code. The owner of the project joins the branch to the appropriate branch.

## Reporting a new task in [_Issue board_](https://gitlab.com/rafal.kociniewski/exchangerate/-/issues).

If you want to report a bug in a project, you have a question about feature, you noticed important aspect is missing in
this type of project or you think this project should've a special feature - create an appropriate `Issue`. This is a
good way of communicating your idea or problem.

### Tips for reporting tasks

* Check if someone has already posted a problem similar to yours. There is no point in duplicating the same
  information (according to the [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principle). If there is a
  closed `issue` with a description of your problem - please report it, and someone will reopen it.
* Describe in details reproducing steps of your problem. You must include information about what behavior you expected
  and what result you got (especially if there was an unclear error message, etc.).
* If you have encountered a bug, you know how to fix, start working on the appropriate branch after reporting it. If you
  don't have sufficient privileges to create new code, describe the solution in detail, someone will be assigned to this
  task.
* Provide information about your system and environment (e.g. operating system, your interaction with the application,
  etc.). All these details allow you to focus on the problem at hand.
* Use labels. This will clearly define the type of issue. The labels currently used are:
    - **bug** - error, defect and other unwanted behavior detected in `master` branch.
    - **devOps** - issue related to development and operations, e.g. changing in GitLab CI.
    - **discussion** - issue for brainstorming.
    - **documentation** - errors in the documentation, missing documentation or required updating of the documentation..
    - **feature** - new feature.
    - **research** - researching a topic, testing the usability of a topic, considering a new technology or changing a
      library or checking tool suitability.
    - **suggestion** - suggest a change in some functionality (such as optimization) or other suggestions.
    - **support** - some function or code supporting the current operations in project. Can be considered a lighter
      version of the Feature.
    - **testing** - issues related to testing, e.g. checking functionality, writing unit and integration tests
* Mark the priority of the issue. This allows you to determine the severity of the problem and work order. Used
  priorities are:
    - **priority: low** - low task priority.
    - **priority: medium** - medium task priority.
    - **priority: high** - high task priority.
* It is a good practice to add a tag to name before actual name, e.g. if the problem is with Gradle, you can
  add `[Gradle]` at start.
* Include the error message you received.

# Code of Conduct

## Our Pledge

We as members, contributors, and leaders pledge to make participation in our
community a harassment-free experience for everyone, regardless of age, body
size, visible or invisible disability, ethnicity, sex characteristics, gender
identity and expression, level of experience, education, socio-economic status,
nationality, personal appearance, race, caste, color, religion, or sexual
identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming,
diverse, inclusive, and healthy community.

## Our Standards

Examples of behavior that contributes to a positive environment for our
community include:

* Demonstrating empathy and kindness toward other people
* Being respectful of differing opinions, viewpoints, and experiences
* Giving and gracefully accepting constructive feedback
* Accepting responsibility and apologizing to those affected by our mistakes,
  and learning from the experience
* Focusing on what is best not just for us as individuals, but for the overall
  community

Examples of unacceptable behavior include:

* The use of sexualized language or imagery, and sexual attention or advances of
  any kind
* Trolling, insulting or derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or email address,
  without their explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

## Enforcement Responsibilities

Community leaders are responsible for clarifying and enforcing our standards of
acceptable behavior and will take appropriate and fair corrective action in
response to any behavior that they deem inappropriate, threatening, offensive,
or harmful.

Community leaders have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct, and will communicate reasons for moderation
decisions when appropriate.

## Scope

This Code of Conduct applies within all community spaces, and also applies when
an individual is officially representing the community in public spaces.
Examples of representing our community include using an official e-mail address,
posting via an official social media account, or acting as an appointed
representative at an online or offline event.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported to the community leaders responsible for enforcement at
[mail](mailto:rafal.kociniewski@gmail.com).
All complaints will be reviewed and investigated promptly and fairly.

All community leaders are obligated to respect the privacy and security of the
reporter of any incident.

## Enforcement Guidelines

Community leaders will follow these Community Impact Guidelines in determining
the consequences for any action they deem in violation of this Code of Conduct:

### 1. Correction

**Community Impact**: Use of inappropriate language or other behavior deemed
unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from community leaders, providing
clarity around the nature of the violation and an explanation of why the
behavior was inappropriate. A public apology may be requested.

### 2. Warning

**Community Impact**: A violation through a single incident or series of
actions.

**Consequence**: A warning with consequences for continued behavior. No
interaction with the people involved, including unsolicited interaction with
those enforcing the Code of Conduct, for a specified period of time. This
includes avoiding interactions in community spaces as well as external channels
like social media. Violating these terms may lead to a temporary or permanent
ban.

### 3. Temporary Ban

**Community Impact**: A serious violation of community standards, including
sustained inappropriate behavior.

**Consequence**: A temporary ban from any sort of interaction or public
communication with the community for a specified period of time. No public or
private interaction with the people involved, including unsolicited interaction
with those enforcing the Code of Conduct, is allowed during this period.
Violating these terms may lead to a permanent ban.

### 4. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community
standards, including sustained inappropriate behavior, harassment of an
individual, or aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within the
community.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage],
version 2.1, available at
[https://www.contributor-covenant.org/version/2/1/code_of_conduct.html][v2.1].

Community Impact Guidelines were inspired by
[Mozilla's code of conduct enforcement ladder][Mozilla CoC].

For answers to common questions about this code of conduct, see the FAQ at
[https://www.contributor-covenant.org/faq][FAQ]. Translations are available at
[https://www.contributor-covenant.org/translations][translations].

[homepage]: https://www.contributor-covenant.org
[v2.1]: https://www.contributor-covenant.org/version/2/1/code_of_conduct.html
[Mozilla CoC]: https://github.com/mozilla/diversity
[FAQ]: https://www.contributor-covenant.org/faq
[translations]: https://www.contributor-covenant.org/translations
